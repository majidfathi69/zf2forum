1. Clone https://github.com/ZF-Commons/ZfcUser project into your ./vendor/ directory

2. Clone https://github.com/ZF-Commons/ZfcBase project into your ./vendor/ directory

3. Enabling it in your application.config.phpfile.

<?php

return array(

    'modules' => array(
    
        // ...
        
        'ZfcUser',
        
        'ZfcBase',
        
    ),
    
    // ...
    
);


4. Download bootstrap 3 via this link http://getbootstrap.com/getting-started#download
 and replace in public directory instead of old files.