<?php

namespace Answer\Model;

use Zend\Db\TableGateway\TableGateway;

class AnswerTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getAnswer($id) {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function saveAnswer(Answer $answer, $user_id) {
        $data = array(
            'description' => $answer->description,
            'user_id' => $user_id,
        );

        $id = (int) $answer->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } else {

            if ($this->getAnswer($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Answer id does not exist');
            }
        }
    }
}