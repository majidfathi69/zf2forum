<?php

namespace Answer\Model;

class Answer {

    public $id;
    public $user_id;
    public $description;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->description = (!empty($data['description'])) ? $data['description'] : null;
    }

}
 