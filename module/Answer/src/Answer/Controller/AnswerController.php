<?php

namespace Answer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Answer\Form\AnswerForm;
use Answer\Model\Answer;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use Zend\Form\Element;
use Discourse\Model\Discourse;

class AnswerController extends AbstractActionController {

    protected $answerTable;
    protected $discourseTable;
    
    public function indexAction() {
        return new ViewModel(array(
            'answers' => $this->getAnswerTable()->fetchAll(),
        ));
    }

    public function addAction() {
        $discourse_id = (int) $this->params()->fromRoute('id', 0);
        
        $discourse_id_elem = new Element\Hidden('discourse_id');
        $discourse_id_elem->setValue($discourse_id);
        
        $form = new AnswerForm();
        $form->add($discourse_id_elem);
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $Answer = new Answer();
            $form->get('submit')->setAttribute('value', 'Add New Answer');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $discourse_id = $request->getPost()->discourse_id;
                $user_id = $this->zfcUserAuthentication()->getIdentity()->getId();
                $Answer->exchangeArray($form->getData());
                $answer_id = $this->getAnswerTable()->saveAnswer($Answer, $user_id);
                
                $discourse = new Discourse();
                $discourse->id = $discourse_id;
                $discourse->answers_id = $answer_id;
                
                $this->getDiscourseTable()->saveDiscourse($discourse);
                
                return $this->redirect()->toRoute("discourse",[
                        'controller' => 'Discourse',
                        'action' => 'view',
                        'id' => $discourse_id,
                    ]
                );
            }
        }

        return array('form' => $form);
    }

    public function editAction() {
        
    }

    public function deleteAction() {
        
    }
    
    public function getAnswerTable() {
    
        if (!$this->answerTable) {
            $sm = $this->getServiceLocator();
            $this->answerTable = $sm->get('Answer\Model\AnswerTable');
        }
    
        return $this->answerTable;
    }

    public function getDiscourseTable() {
        if (!$this->discourseTable) {
            $sm = $this->getServiceLocator();
            $this->DiscourseTable = $sm->get('Discourse\Model\DiscourseTable');
        }
        return $this->DiscourseTable;
    }
    
}
