<?php

namespace Answer\Form;

use Zend\Form\Form;

class AnswerForm extends Form {

    public function __construct($name = null) {

        parent::__construct('Answer');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'description',
            'type' => 'Textarea ',
            'options' => array(
                'label' => 'Description',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }

}
