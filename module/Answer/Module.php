<?php

namespace Answer;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Answer\Model\Answer;
use Answer\Model\AnswerTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {

        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Answer\Model\AnswerTable' =>   function($sm) {
                    $tableGateway = $sm->get('AnswerTableGateway');
                    $table = new AnswerTable($tableGateway);
                    return $table;
                },
                'AnswerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Answer());
                    return new TableGateway('answer', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

}