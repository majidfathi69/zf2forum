<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Answer\Controller\Answer' => 'Answer\Controller\AnswerController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'answer' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/answer[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'answer\Controller\answer',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'answer' => __DIR__ . '/../view',
        ),
    ),
);