<?php

namespace Discourse;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Discourse\Model\Discourse;
use Discourse\Model\DiscourseTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
        'factories' => array(
        'Discourse\Model\DiscourseTable' =>   function($sm) {
            $tableGateway = $sm->get('DiscourseTableGateway');
            $table = new DiscourseTable($tableGateway);
            return $table;
        },
        'DiscourseTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Discourse());
            return new TableGateway('discourse', $dbAdapter, null, $resultSetPrototype);

        },
        ),
        );
    }

}