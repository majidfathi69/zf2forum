<?php

namespace Discourse\Model;

class Discourse {

    public $id;
    public $user_id;
    public $question_id;
    public $answers_id;
    public $correct_answer_id;
    public $view_num;

    public function exchangeArray($data) {

        $this->id = (!empty($data['id'])) ? $data['id'] : null;

        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;

        $this->question_id = (!empty($data['question_id'])) ? $data['question_id'] : null;

        $this->answers_id = (!empty($data['answers_id'])) ? $data['answers_id'] : null;
        
        $this->correct_answer_id = (!empty($data['correct_answer_id'])) ? $data['correct_answer_id'] : null;

        $this->view_num = (!empty($data['view_num'])) ? $data['view_num'] : null;
    }

}
 