<?php

namespace Discourse\Model;

use Zend\Db\TableGateway\TableGateway;

class DiscourseTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getDiscourse($id) {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveDiscourse(Discourse $discourse) {
        
        if($discourse->id){
            $_discourse = $this->getDiscourse($discourse->id);
            
            if($discourse->answers_id != ''){
                if($_discourse->answers_id != ''){
                    $_discourse->answers_id .= ',' . $discourse->answers_id;
                }else{
                    $_discourse->answers_id = $discourse->answers_id;
                }
            }
            $data = array(
                'answers_id' => $_discourse->answers_id,
                'correct_answer_id' => $_discourse->correct_answer_id,
                'view_num' => $_discourse->view_num,
            );
        }

        $id = (int) $discourse->id;

        if ($id == 0) {
            $data = array(
                'question_id' => $discourse->question_id,
                'user_id' => $discourse->user_id,
            );
            $this->tableGateway->insert($data);
        } else {
            if ($this->getDiscourse($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('getDiscourse id does not exist');
            }
        }
    }
}
