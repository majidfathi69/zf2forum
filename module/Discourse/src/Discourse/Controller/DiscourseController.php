<?php

namespace Discourse\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DiscourseController extends AbstractActionController {

    protected $discourseTable;
    protected $questionTable;
    protected $answerTable;
    
    public function indexAction() {
        $discourses = $this->getDiscourseTable()->fetchAll();
        $questions = [];
        
        foreach($discourses as  $discourse){
            $question = $this->getQuestionTable()->getQuestion($discourse->question_id);
            $question->discourse_id = $discourse->id;
            $questions[] = $question;
        }
        
        return new ViewModel(array(
            // 'discourses' => $this->getDiscourseTable()->fetchAll(),
            'questions' => $questions,
        ));
    }

    public function addAction() {
        
    }

    public function editAction() {
        
    }

    public function deleteAction() {
        
    }

    public function getDiscourseTable() {
        if (!$this->discourseTable) {
            $sm = $this->getServiceLocator();
            $this->DiscourseTable = $sm->get('Discourse\Model\DiscourseTable');
        }
        return $this->DiscourseTable;
    }
    
    public function getQuestionTable() {
        if (!$this->questionTable) {
            $sm = $this->getServiceLocator();
            $this->QuestionTable = $sm->get('Question\Model\QuestionTable');
        }
        return $this->QuestionTable;
    }
    
    public function getAnswerTable() {
        if (!$this->snswerTable) {
            $sm = $this->getServiceLocator();
            $this->AnswerTable = $sm->get('Answer\Model\AnswerTable');
        }
        return $this->AnswerTable;
    }
    
    public function viewAction() {
        $discourse_id = (int) $this->params()->fromRoute('id', 0);
        $discourse= $this->getDiscourseTable()->getDiscourse($discourse_id);
        $question = $this->getQuestionTable()->getQuestion($discourse->question_id);
        
        $answers_id_str = $discourse->answers_id;
        $answers_id = explode(',', $answers_id_str);
        
        foreach($answers_id as $answer_id){
            if($answer_id == '') break;
            $answer = $this->getAnswerTable()->getAnswer($answer_id);
            
            $answers[] = $answer;
        }
        /*if (!$id) {
            return $this->redirect()->toRoute('questions', array(
                'action' => 'add'
            ));
        }*/

        // Get the Questions with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        /*try {
            $Question = $this->getQuestionsTable()->getQuestion($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('discourse', array(
                'action' => 'index'
            ));
        }*/
        
        return array(
            'id' => $discourse_id,
            'question' => $question,
            'answers' => $answers,
        );
    }
}
