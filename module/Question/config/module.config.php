<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Question\Controller\Question' => 'Question\Controller\QuestionController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'question' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/question[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Question\Controller\Question',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'question' => __DIR__ . '/../view',
        ),
    ),
);
