<?php

namespace Question\Model;

use Zend\Db\TableGateway\TableGateway;

class QuestionTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function fetchAllMine($user_id) {
        $resultSet = $this->tableGateway->select(['user_id' => $user_id]);
        return $resultSet;
    }
    
    public function getQuestion($id) {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveQuestion(Question $question, $user_id) {
        $data = array(
            'user_id' => $user_id,
            'title' => $question->title,
            'description' => $question->description,
        );

        $id = (int) $question->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } else {
            if ($this->getQuestion($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Questions id does not exist');
            }
        }
    }
}

