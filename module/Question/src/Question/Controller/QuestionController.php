<?php

namespace Question\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Question\Form\QuestionForm;
use Question\Model\Question;
use Discourse\Model\Discourse;

class QuestionController extends AbstractActionController {

    protected $questionTable;
    protected $answerTable;
    
    public function indexAction() {
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            $user_id = $this->zfcUserAuthentication()->getIdentity()->getId();
            return new ViewModel(array(
                'questions' => $this->getQuestionTable()->fetchAllMine($user_id),
            ));
        }else{
            return new ViewModel(array(
                'questions' => $this->getQuestionTable()->fetchAll(),
            ));
        }
        
    }

    public function addAction() {
        $form = new QuestionForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $questions = new Question();
            $form->get('submit')->setAttribute('value', 'Add New Question');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $questions->exchangeArray($form->getData());
                $user_id = $this->zfcUserAuthentication()->getIdentity()->getId();
                $question_id = $this->getQuestionTable()->saveQuestion($questions, $user_id);
                
                $discourse = new Discourse();
                $discourse->question_id = $question_id;
                $discourse->user_id = $user_id;
                
                $this->getDiscourseTable()->saveDiscourse($discourse);
                
                // Redirect to list of questions
                return $this->redirect()->toRoute('question');
            }
        }

        return array('form' => $form);
    }

    public function editAction() {
        
    }

    public function deleteAction() {
        
    }
    
    public function getQuestionTable() {
    
        if (!$this->questionTable) {
            $sm = $this->getServiceLocator();
            $this->questionTable = $sm->get('Question\Model\QuestionTable');
        }
        return $this->questionTable;
    }
    
    
    public function getDiscourseTable() {
        if (!$this->discourseTable) {
            $sm = $this->getServiceLocator();
            $this->DiscourseTable = $sm->get('Discourse\Model\DiscourseTable');
        }
        return $this->DiscourseTable;
    }
}